<?php
require_once('path.php');
require_once('database.php');
require_once('startsession.php');
if (!isset($_SESSION['id'])) 
{
    header('Location: http://student.agh.edu.pl/milqaa/signin.php');
}
$dbc = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE)
or die ("Nie można połączyć się z bazą danych");
if (isset($_GET['id']))
{
	$id = $_GET['id'];
	$query = "SELECT * FROM Serwis WHERE id = $id";
	$result = mysqli_query($dbc, $query)
	or die ("Błąd w zapytaniu.");
	$user = mysqli_fetch_array($result);
	$page_title = $user['first_name'] . ' ' . $user['last_name'];
	require_once('header.php');
	if ($user['image'] != NULL)
	echo '<img src = "' . GW_UPLOADPATH . $user['image'] . '" alt = "Awatar" /><br />';
	else
	echo '<img src = "' . GW_UPLOADPATH . 'noprofile.jpg" alt = "Awatar" /><br />';
	echo 'Użytkownik: ' . $user['first_name'] . ' ' . $user['last_name'] . '<br />';
	echo 'Data urodzenia: ' . $user['birth_date'] . '</br>'; 
}
else
{
	header('Location: http://student.agh.edu.pl/milqaa/index.php');
}
if ($_SESSION['id'] == $user['id'])
{
?>
<input type="button" value="Edytuj profil" onclick="parent.location.href='http://student.agh.edu.pl/milqaa/edit.php'"></br>
<input type="button" value="Usuń konto" onclick="parent.location.href='http://student.agh.edu.pl/milqaa/remove.php'"></br>
<?php
}
?>
<input type="button" value="Powrót do strony głównej" onclick="parent.location.href='http://student.agh.edu.pl/milqaa'"></br>
<?php
require_once('footer.php');
?>