<?php
$error_msg = '';
require_once('database.php');
$page_title = "rejestracja";
require_once('header.php');
$dbc = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE)
or die ("Nie można się połączyć.");
$dbc -> query ('SET NAMES utf8');
$dbc -> query ('SET CHARACTER_SET utf8_unicode_ci');
  if (isset($_POST['submit'])) {
    // Pobieranie danych do profilu z żądania POST.
    $username = mysqli_real_escape_string($dbc, trim($_POST['username']));
    $password1 = mysqli_real_escape_string($dbc, trim($_POST['password1']));
    $password2 = mysqli_real_escape_string($dbc, trim($_POST['password2']));
	$first_name = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
	$last_name = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
	$email = mysqli_real_escape_string($dbc, trim($_POST['email']));
	$birth_date = mysqli_real_escape_string($dbc, trim($_POST['birth_date']));
	$sex = mysqli_real_escape_string($dbc, trim($_POST['sex']));
	$agree = mysqli_real_escape_string($dbc, trim($_POST['agree']));
	$join_date = date('Y-m-d');
  if (!empty($username) && !empty($password1) && !empty($password2) && !empty($first_name) && !empty($last_name) &&
  !empty($email) && !empty($birth_date) && ($password1 == $password2) && !empty($sex) && ($agree == 'yes'))
  {
		$is_username = "SELECT * FROM Serwis WHERE username = '$username'";
		$data = mysqli_query($dbc, $is_username)
		or die ("Błąd w zapytaniu.");
		if (mysqli_num_rows($data) == 0)
		{
		  $query = "INSERT INTO Serwis(username, password, first_name, last_name, email, birth_date, sex, join_date) 
		  VALUES ('$username',SHA('$password1'),'$first_name','$last_name','$email','$birth_date','$sex','$join_date')";
		  mysqli_query($dbc, $query)
		  or die ("Błąd w zapytaniu");
		  echo "Zostałeś zarejestrowany.";
		  $code = sha1($email);
		  $naglowki = "Reply-to: milqaa@student.agh.edu.pl <milqaa@student.agh.edu.pl>".PHP_EOL;
		  $naglowki .= "From: milqaa@student.agh.edu.pl <milqaa@student.agh.edu.pl>".PHP_EOL;
          $naglowki .= "MIME-Version: 1.0".PHP_EOL;
          $naglowki .= "Content-type: text/html; charset=UTF-8".PHP_EOL; 
		  $link = 'http://student.agh.edu.pl/milqaa/approve.php?username=' . $username. '&code=' . $code;
		  $delete = 'http://student.agh.edu.pl/milqaa/delete.php?username=' . $username . '&code=' . $code;
		  $msg = '<html><head></head><body><h3>Witaj, ' . $first_name . ' ' . $last_name . ',</h3><br />
		  zarejestrowałeś się przed chwilą w serwisie Gabinder.<br />
		  Aby potwierdzić rejestrację, kliknij poniższy link: <br /><a href="' . $link . '">Potwierdź</a> 
		  <br />Jeśli to nie Ty się zarejestrowałeś, kliknij: <br /><a href="' . $delete . '">Skasuj maila z bazy</a><br />
		  Pozdrawiamy,<br />zespół Gabinder.</body></html>';
		  mysqli_close($dbc);
		  mail($email, 'Rejestracja w Gabinder', $msg, $naglowki);
		  ?>
		  <input type="button" value="Powrót do strony głównej." onclick="parent.location.href='http://student.agh.edu.pl/milqaa'">
		  <?php
		  exit();
		}
		else
		{
			$error_msg = "Podana nazwa użytkownika jest już zajęta. Wprowadź inną.";
			$username = "";
		}
		  
  }
  else if ($password1!=$password2)
  {
	  $error_msg = "Hasła muszą być zgodne.";
  }
  else if ($agree == 'no')
  {
	  $error_msg = "Nie wyraziłeś zgody na rejestrację.";
  }
  else
  {
	  $error_msg = "Wypełnij wszystkie pola.";
  }
 }
 ?>
 <h2>Zarejestruj się: </h2>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <fieldset>
    <legend>Dane do rejestracji</legend>
	<label for="username">Login:</label>
    <input type="text" id="username" name="username" value= "<?php echo $username; ?>" /><br />
	<label for="password1">Hasło:</label>
    <input type="password" id="password1" name="password1" /><br />
	<label for="password2">Powtórz hasło:</label>
    <input type="password" id="password2" name="password2"  /><br />
    <label for="firstname">Imię:</label>
    <input type="text" id="first_name" name="first_name" value= "<?php echo $first_name; ?>"/><br />
    <label for="lastname">Nazwisko:</label>
    <input type="text" id="last_name" name="last_name" value= "<?php echo $last_name; ?>" /><br />
    <label for="email">Adres e-mail:</label>
    <input type="text" id="email" name="email" value= "<?php echo $email; ?>"/><br />
	<label for="birth_date">Data urodzenia</label>
	<input type="date" id="birth_date" name="birth_date" value = "<?php echo $birth_date; ?>" /></br>
    <label for="sex">Płeć</label> </br>
    K <input id="sex" name="sex" type="radio" value="k" />
    M <input id="sex" name="sex" type="radio" value="m" checked="checked" /><br />
	<label for="agree">Zgoda na rejestrację</label> </br>
    Tak <input id="agree" name="agree" type="radio" value="yes" />
    Nie <input id="agree" name="agree" type="radio" value="no" checked="checked" /><br />
	</fieldset>
	<input type="submit" value="Zarejestruj się" name="submit" />
	</form>
<?php
echo '<p class="error">' . $error_msg . '</p>';
require_once('footer.php');
?>