<?php
$page_title = "logowanie";
require_once('header.php');
require_once('database.php');
require_once('path.php');
session_start();
$error_msg = "";
if (!isset($_SESSION['id']))
{	
	if (isset($_POST['submit']))
	{
		$dbc = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, DATABASE)
		or die("Brak połączenia z bazą danych.");
		$username = mysqli_real_escape_string($dbc, trim($_POST['username']));
		$password = mysqli_real_escape_string($dbc, trim($_POST['password']));
		if (!empty($username) && (!empty($password)))
		{
			$query="SELECT * FROM Serwis WHERE username = '$username'";
			$result = mysqli_query($dbc, $query)
			or die("Błąd w zapytaniu.");
			$row = mysqli_fetch_array($result);
			$tmp_username = $row['username'];
			if (!empty($tmp_username))
			{
				$query="SELECT * FROM Serwis WHERE username = '$username' AND password = SHA('$password')";
				$data = mysqli_query($dbc, $query)
				or die("Błąd w zapytaniu.");
				if (mysqli_num_rows($data) == 1)
				{
					$row = mysqli_fetch_array($data);
					if($row['approved'] == 1)
					{
						$_SESSION['id'] = $row['id'];
						$_SESSION['username'] = $row['username'];
						setcookie('id', $row['id'], time() + (60 * 60 * 24 * 30));    // Wygasa za 30 dni.
						setcookie('username', $row['username'], time() + (60 * 60 * 24 * 30));  // Wygasa za 30 dni.
						header('Location: http://student.agh.edu.pl/milqaa/');
					}
					else
					{
						$error_msg = "Proszę najpierw potwierdzić konto.";
						header('Refresh: 1; http://student.agh.edu.pl/milqaa/');
					}
					
				}
				else
				{
					$error_msg = "Podałeś błędne hasło.";
				}
			}
			else
			{
				$error_msg = "Użytkownika o takim loginie nie ma w tym serwisie.";
			}
		}
		else
		{
			$error_msg = "Podaj wszystkie dane.";
		}
	}
}
if(empty($_SESSION['id']))
{
?>
<h1> Zaloguj się </h1>
<form method ="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<fieldset>
<label for="username">Login</label>
<input type="text" id="username" name="username" value="<?php echo $tmp_username; ?>"/></br>
<label for="password">Hasło</label>
<input type="password" id="password" name="password"/></br>
</fieldset>
<input type="submit" value="Zaloguj się" name="submit"/>
</form>
<?php
}
echo '<p class ="error">' . $error_msg . '</p>';
require_once('footer.php');
?>
